package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmployeeController {

    @RequestMapping(path="/", method=RequestMethod.GET)
    public String goHome(){
        return "ColResize";
    }

    @RequestMapping(path="/index", method=RequestMethod.GET)
    public String goIndex(){
        return "index";
    }

    @RequestMapping(path="/index2", method=RequestMethod.GET)
    public String goIndex2(){
        return "index2";
    }

    @RequestMapping(path="/DomPosition", method=RequestMethod.GET)
    public String DomPosition(){
        return "DomPosition";
    }

    @RequestMapping(path="/FlexibleTableWidth", method=RequestMethod.GET)
    public String FlexibleTableWidth(){
        return "FlexibleTableWidth";
    }

    @RequestMapping(path="/StateSave", method=RequestMethod.GET)
    public String StateSave(){
        return "StateSave";
    }

    @RequestMapping(path="/AlternativePagination", method=RequestMethod.GET)
    public String AlternativePagination(){
        return "AlternativePagination";
    }

    @RequestMapping(path="/ScrollVertical", method=RequestMethod.GET)
    public String ScrollVertical(){
        return "ScrollVertical";
    }

    @RequestMapping(path="/ColResize", method=RequestMethod.GET)
    public String ColResize(){
        return "ColResize";
    }

}