$(document).ready( function () {
    var table = $('#employeesTable').DataTable({
        "sAjaxSource": "/employees",
        "sAjaxDataProp": "",
        "order": [[ 0, "asc" ]],
        //"paging":   false,
        "ordering": true,
        "info":     false,
        // stateSave: true,
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "dom": 'rt<"bottom"iflp<"clear">>',

        // "pagingType": "full_numbers",
        // "pagingType": "numbers",
        // "pagingType": "simple",
        // "pagingType": "simple_numbers",
        // "pagingType": "full_numbers",
        // "pagingType": "first_last_numbers",
        // "scrollY":        "200px",
        // "scrollCollapse": true,
        "paging":         true,
        "aoColumns": [
            { "mData": "id"},
            { "mData": "name" },
            { "mData": "lastName" },
            { "mData": "email" },
            { "mData": "phone" },
            { "mData": "active" }
        ],

        "columnDefs": [
            {
                "targets": [ 2 ],
                "visible": true,
                "searchable": false
            },
            {
                "targets": [ 3 ],
                "visible": true
            }
        ]
    });

    //使用col插件实现表格头宽度拖拽
    $("#employeesTable").colResizable({
        liveDrag:true
    });

    // $("#employeesTable").colResizable({
    //  liveDrag:true,
     // gripInnerHtml:"<div class='grip'></div>",
     // draggingClass:"dragging",
     // resizeMode:'fit'
     // });


});