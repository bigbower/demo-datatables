$(document).ready( function () {
    var table = $('#employeesTable').DataTable({
        "sAjaxSource": "/employees",
        "sAjaxDataProp": "",
        "order": [[ 0, "asc" ]],
        //"paging":   false,
        "ordering": true,
        "info":     false,
        stateSave: true,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "aoColumns": [
            { "mData": "id"},
            { "mData": "name" },
            { "mData": "lastName" },
            { "mData": "email" },
            { "mData": "phone" },
            { "mData": "active" }
        ],
        "columnDefs": [
            {
                "targets": [ 2 ],
                "visible": true,
                "searchable": false
            },
            {
                "targets": [ 3 ],
                "visible": true
            }
        ]
    })
});